public class Lab3
{
    public static void main(String[] args)
    {
        int a=200,b=30,c=140;

        if(a>b && a>c)
        {
            System.out.println(a+" is largest number");
            if(b>c)
            {
                System.out.println(b+" is second largest number");
            }
            else
            {
                System.out.println(c+" is second largest number");
            }
        }
        else if(b>a && b>c)
        {
            System.out.println(b+" is largest number");
            if(a>c)
            {
                System.out.println(a+" is second largest number");
            }
            else
            {
                System.out.println(c+" is second largest number");
            }
        }
        else
        {
            System.out.println(c+" is largest number");
            if(a>b)
            {
                System.out.println(a+" is second largest number");
            }
            else
            {
                System.out.println(b+" is second largest number");
            }
        }
    }
}
